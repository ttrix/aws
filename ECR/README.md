# ECR (Elastic Container Registry)

A service providing storage
Collection of repositories
A repository per image. Important: We don't have one repo where we can push multiple images but rather for each image we have its own repository.

Collection of related images with smae name, but different versions

## Create a repository

Search for ECR -> Create repository -> set a name

## Push image to a repository

To visualize the list of commands needed to push an image:

select repo -> click View push commands

We will get the different specific commands to:

- authenticate in AWS
- build image
- create tag
- push image

## Create user with read and write permissions to EC2 (AmazonEC2ContainerRegistryFullAccess policy)

-> Go to IAM service
-> click Users option in left menu
-> click Create User button
-> set name
-> select "Add user to group"

(create a group if none)
-> click Create group button
-> set group name
-> select **AmazonEC2ContainerRegistryFullAccess**
-> click Create user group button

-> select the group with the right policy
-> next button
-> Create user button
