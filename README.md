# AWS notes

Infrastructure as a Service

## Manage users & permissions with IAM (Identity and Access Management)

Manage access to AWS services and resources
Create and manage AWS users and groups
Assign policies (set of permissions)

Good practice: create Admin user to manage the whole AWS account instead of using Root user

IAM Users: Human or system users
A user delegate tasks to a service but a user cannot assign permissions (policies) to a AWS service so we need to use IAM Roles, assigning a services a roles and policies to that role

Creating a role needs 2 steps:

- Assign a Role to AWS service
- Attach policies to that role

### Create IAM user

#### Admin user

- Users
- Create user
- Provide access to either or both AWS Management Console and AWS Command Line Interface

## Regions & Avalaibility zones

AWS datacenters present in differents physical locations (regions)
Host your applications, where your customers are located

## Configure our own private network VPC (Virtual Private Cloud)

For each Region, we will have a VPC. Our own private/isolated network in the cloud
VPC are set up out-of-the-box
For each availability zone we have one subnet (subnetwork or component of our network)

Subnets are private or public depending on the Firewall Rules configuration applied
Internal IP range can be changed (this is not for outsite web traffic)

### Security section

NACL: allow configuring access on subnet level
Security Group: Configure access on instance level
Create on VPC level and assign to subnet and instance

### CIDR blocks

Range of IP addresses

## Create virtual servers with EC2 (Elactic Compute Cloud)

- Virtual server in AWS cloud

## Continuous Deployment with Jenkins

### Build pipeline to deploy to EC2 instance

Build app - build image - push to docker repo - deploy to doker repo - deploy to AWS EC2

## introduction to AWS CLI
