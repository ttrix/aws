# Create virtual servers with EC2 (Elactic Compute Cloud)

- Virtual server in AWS cloud

## Demo project - Deploy Web application on EC2 instance

### Create an EC2 instance on AWS

EC2 / Launch Instance

The Amazon Linux distribution is a version optimized by Amazon to be more performant on AWS. Based on RHLE distribution.

#### Steps to create an instance

- Add tags
- Choose OS image
- Choose capacity
- Network configurations
- Configure Security Group
- Add storage

### Connect to EC2 instance with ssh

- Create key pairs on AWS
  Private key file format: .pem (Linux or Mac), .ppk (Windows)
- Download private key locally
- Public key stored by AWS

Example:

```shell
$ ssh -i ~/.ssh/aws-docker-server.pem ec2-user@<Public IPv4 address>
```

### Install Docker on remote EC2 instance

Install docker

```shell
$ sudo yum update
$ sudo yum install docker
```

Start docker daemon

```shell
$ sudo service docker start
```

chec docker is running

```shell
$ ps aux | grep docker
```

### Run docker container (docker login, pull, run) from private repo

Remove need to use _sudo command_ to run _docker commands_ by adding our current user ($USER) to **docker group**

```shell
$ sudo usermod -aG docker $USER
```

Then we need to logout-login for the changes to be applied
Check the user is now part of **docker group**

```shell
$ groups
```

### Run Web Application on EC2

Local environment:

- Build docker image (for demo-project/react-nodejs-example)
- Push the image to private DockerHub Repository

EC2 instance:

- Pull docker image
  docker pull dockerhub-repo/repo-name:tag
- Run docker container
  docker run -p 3000:3080 dockerhub-repo/repo-name:tag

### Configure EC2 Firewall to access app externally from browser

- Go to Intances, select instance
- Security tab/section
- Click on the Security group
- Edit Inbound rules (traffic into the server) and (if needed) edit Outbound rules (traffic leaving the server)
- Add rule: Type (Custom TCP) , Port (3000), Source (0.0.0.0/0 means anywhere)
